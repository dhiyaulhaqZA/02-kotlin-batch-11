fun main(args: Array<String>) {
    //val or var [nama variablenya]: typeData = isinya

    var x = 5
    val y = 2
    x = 1

    var a: Double = 1123412341234.223423
    var b: Float = 1.2f

    var l: Long = 123_412_341_234
    var i: Int = 1234123412

    val isStudent: Boolean = 1 + 1 != 1
    val isActive: Boolean = 1 + 1 == 1

    //cara manggil fungsi
    //namaFungsi(parameter)

//    println(..) print + nambah baris
//    print(..) print saja

    val angka = 50
    val number = 40
    print("Angka : $angka")
    print("Angka : ${angka + number}")
    println("Penjumlahan : ${1 + 1})")
    println("Penjumlahan : ${penjumlahan(1, 2)}")
    println("Pengurangan : ${pengurangan(3, 1)}")
    println("Perkalian : ${perkalian(3, 1)}")
    println("Pembagian : ${pembagian(6, 2)}")
    println("Name : ${getName()}")
    println("Name : ${getName("Ulhaq")}")
    println("a == 2 : ${isTrue(1)}")
    println("sum : ${sum(2, 3)}")
    println("sum : ${sum(2)}")
    println("pengurangan: ${subtraction(b = 1, a = 2)}")
}

fun subtraction(a: Int, b: Int) = a - b

fun isTrue(a: Int): Boolean {
    return a == 2
}

fun sum(a: Int, b: Int = 2) = a + b

// contoh parameter dengan default value
// name: String = ""

fun getName(name: String = "Nama"): String {
    return name
}

fun pembagian(a: Int, b: Int): Int {
    return a / b
}

fun perkalian(a: Int, b: Int): Int {
    return a * b
}

fun pengurangan(a: Int, b: Int): Int {
    return a - b
}

fun penjumlahan(a: Int, b: Int) = a + b
